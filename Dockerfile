# Imagen inicial a partir de la cual creamos la imagen
FROM node

# Definimos directorio del contenedor
WORKDIR /colapi_mm

# añadios contenido del proyecto al directorio del contenedor
ADD . /colapi_mm

# Puerto de escucha del contenedor
EXPOSE 3000

# Comandos para lanzar nuestra API REST
CMD ["npm", "start"]
# CDM ["node", "server_v2.js"]
