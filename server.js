var express = require('express');
var bodyParser = require("body-parser");
var app = express();
var port = process.env.PORT || 3000;
var users_file = require('./users.json');
var URLbase = "/colapi/v1/";

app.listen(port);
app.use(bodyParser.json());
console.log("Colapi escuchando en puerto " + port);

// peticion GET con parametros
app.get(URLbase + 'users/:id/:otro',
 function (req, res) {
   console.log('GET /colapi/v1/users/:id');
   console.log('req.params.otro: ' + req.params.otro);
   var pos = req.params.id;
   var respuesta = req.params.id + "," + req.params.otro;
   res.send(req.params);
});

//peticiòn GET con query string (req.query)
app.get(URLbase + 'users',
  function(peticion,respuesta){
    console.log("GET con query string");
    console.log(peticion.query.id);
    console.log(peticion.query.country);
    respuesta.send({"msg":"GET con query string"});
  });

// peticiòn POST
  app.post(URLbase + 'users',
    function(req,res){
      console.log("POST correcto");
      console.log(req.body);
      var jsonID = {};
      var newid = users_file.length + 1;
        jsonID = req.body;
        jsonID.id = newid;
    //    users_file.push(newid);
    //    users_file.push(req.body);
    users_file.push(jsonID);
      res.send({"msg":"Usuario creado correctamente","id: ": jsonID})
    //  res.send(users_file);
    //  res.send({"msg":"Peticiòn POST resuelta"});
    });

// peticiòn PUT
      app.put(URLbase + 'users/:id',
        function(req,res){
          console.log("Peticiòn PUT ...");
          var idBuscar = req.params.id;
          var updateUser = req.body;
          console.log("Este es el body "+ req.body);
          for (var i; i< users_file.length; i++){
            if (users_file[i].id == idBuscar){
              res.send({"msg": "Usuario Actualizado Correctamente", updateUser});
            }
            }
          });

// peticiòn DELETE
    app.delete(URLbase + 'users/:id',
      function(req,res){
        console.log("Peticiòn DELETE ...");
        var idBuscar = req.params.id;
          for (var i; i< users_file.length; i++){
             if (users_file[i].id == idBuscar){
                var deleteUser = req.params.id;
                console.log("Ingresa al IF", deleteUser);
                res.send({"msg": "Usuario Actualizado Correctamente", deleteUser});
                var i = users_file.length;
            }
              }
      });
