var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../server_v2')

var should = chai.should()

chai.use(chaiHttp)

describe('Pruebas Colombia', () => {
  it('Test BBVA ', (done) => {
    chai.request('http://www.bbva.com')
        .get('/')
        .end((err,res) => {
          console.log(res)
          res.should.have.status(200)
              done();
        })
    });
   it('Mi API funciona ', (done) => {
     chai.request('http://localhost:3000')
         .get('/colapi/v3/users')
         .end((err,res) => {
           console.log(res.body)
           res.body.should.be.a('array')
               done();
         })
  });
  it('Mayor a 100 ', (done) => {
    chai.request('http://localhost:3000')
        .get('/colapi/v3/users')
        .end((err,res) => {
          console.log(res.body)
          res.body.length.should.be.gte(100)
              done();
        })
 });
  it('Validar primer elemento ', (done) => {
    chai.request('http://localhost:3000')
        .get('/colapi/v3/users')
        .end((err,res) => {
          console.log(res.body[0])
          res.body[0].should.have.property('first_name')
          res.body[0].should.have.property('last_name')
              done();
        })
    });
 it('Crear un elemento', (done) => {
   chai.request('http://localhost:3000')
       .post('/colapi/v3/users')
       .send('{"first_name": "Camila","last_name":"Perez"}')
       .end((err,res,body) => {
         console.log(res)

        done();
       })
   });
});
