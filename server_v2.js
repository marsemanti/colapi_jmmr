var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = "/colapi/v3/";

var baseMLabURL = 'https://api.mlab.com/api/1/databases/colapidb_mm/collections/';
var apiKeyMLab = 'apiKey=QgPz-JRX-aXajKCsKMFdpU9wHVrzL3gg';

app.listen(port, function(){
 console.log("API apicol escuchando en el puerto " + port + "...");
});

app.use(bodyParser.json());

// GET users mLab
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET /colapi/v3/users");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?'+ queryString + apiKeyMLab,
    function(err,resMlab,body){
       console.log('Error: '+err);
       console.log('RespuestaMLab: '+ resMlab);
       console.log('body: '+body);
       var respuesta = body;
       respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
       res.send(respuesta);
    });
});

// Petición GET user con parámetros
app.get(URLbase + 'users/:id',
 function (req, res) {
   console.log("GET /colapi/v3/users/:id");
   console.log(req.params.id);
   var id = req.params.id;
   var queryString = 'q={"id":'+ id +'}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('user?'+queryString+apiKeyMLab,
   function(err,resMlab,body){
     console.log("Respuesta mLab correcta");
     var respuesta = body;
     respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
     res.send(respuesta);
   } )

});


// GET accountss mLab
app.get(URLbase + 'accounts',
 function(req, res) {
   console.log("GET /colapi/v3/accounts");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado");
   var queryString = 'f={"_id":0}&';
   httpClient.get('account?'+ queryString + apiKeyMLab,
    function(err,resMlab,body){
       console.log('Error: '+err);
       console.log('RespuestaMLab: '+ resMlab);
       console.log('body: '+body);
       var respuesta = body;
       respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
       res.send(respuesta);
    });
});

// Petición GET accounts con parametros
app.get(URLbase + 'accounts/:userid',
 function (req, res) {
   console.log("GET /colapi/v3/accounts/:userid");
   console.log(req.params.userid);
   var id = req.params.userid;
   var queryString = 'q={"userid":'+ id +'}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('account?'+queryString+apiKeyMLab,
   function(err,resMlab,body){
     console.log("Respuesta mLab correcta");
     var respuesta = body;
     respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
     res.send(respuesta);
   } )

});

// GET movements mLab
app.get(URLbase + 'movements',
 function(req, res) {
   console.log("GET /colapi/v3/movements");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado");
   var queryString = 'f={"_id":0}&';
   httpClient.get('movement?'+ queryString + apiKeyMLab,
    function(err,resMlab,body){
       console.log('Error: '+err);
       console.log('RespuestaMLab: '+ resMlab);
       console.log('body: '+body);
       var respuesta = body;
       respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
       res.send(respuesta);
    });
});

// Petición GET movements con parametros
app.get(URLbase + 'movements/:id_mov',
 function (req, res) {
   console.log("GET /colapi/v3/movements/:id_mov");
   console.log(req.params.id_mov);
   var id = req.params.id_mov;
   var queryString = 'q={"id_mov":'+ id +'}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('movement?'+queryString+apiKeyMLab,
   function(err,resMlab,body){
     console.log("Respuesta mLab correcta");
     var respuesta = body;
     respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
     res.send(respuesta);
   } )
});

// Petición POST insertar registro nuevo
app.post(URLbase + 'users',
 function (req, res) {
   console.log("POST /colapi/v3/users/");
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('user?'+ apiKeyMLab,
    function(err,resMlab,body){
      console.log("body",body);
       var maxdb = body.length;
   var newID = maxdb + 1;
   var newUser = {
     "id" : newID,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "Password" : req.body.Password
   };
   var httpClient = requestJSON.createClient(baseMLabURL+"/user?"+apiKeyMLab);
   httpClient.post('', newUser,
   function(err,resMlab,body){
     console.log("Respuesta mLab correcta");
     var respuesta = body;
     respuesta = !err ? body : {"msg":"error al realizar Loggin en mlab"}
     res.send(respuesta);
    });
  });
});


// Petición Loggin
app.post(URLbase + 'Loggin',
 function (req, res) {
   console.log("POST /colapi/v3/users/");
   var email = req.body.email;
   console.log(req.body.email);
   var pass = req.body.Password;
   var queryString = 'q={"email":'+ "'" +email+ "'" +'}&';
   console.log(queryString)
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('user?'+queryString+apiKeyMLab,
   function(err,resMlab,body){
     console.log(httpClient);
     console.log("body  ....", body);
     console.log("email body ", body[0].email);
      if (body[0].email == email){
        console.log("email ", email);
        if(body[0].Password == pass){
          console.log("pass", pass);
           body[0].logged = true;
        }
      }
   var httpClient = requestJSON.createClient(baseMLabURL+"/user?");
   console.log("httpClient ", httpClient);
   var cambio = '{"$set":' + JSON.stringify(body[0]) + '}';
   console.log("cambio ", cambio);
   httpClient.put('?q={"email": ' + "'"+req.body.email+ "'" + '}&' + apiKeyMLab, JSON.parse(cambio),
     function(err,resMlab,body){
     console.log("Respuesta mLab correcta");
     console.log("err ", err);
     console.log("body ", body);
     var respuesta = body;
     respuesta = !err ? body: {"msg":"error en la carga de movement en mlab"}
     res.send(respuesta);
    });
  });
});

// Petición logout
app.post(URLbase + 'logout',
 function (req, res) {
   console.log("POST /colapi/v3/users/");
   var id = req.body.id;
   var pass = req.body.Password;
   var queryString = 'q={"id":'+ id +'}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('user?'+queryString+apiKeyMLab,
   function(err,resMlab,body){
      if (body[0].id == id){
        console.log("id ", id);
        if(body[0].logged == true){
          console.log("pass", pass);
        }
      }
   var eliminar = {
        "logged": true
      };
   var httpClient = requestJSON.createClient(baseMLabURL+"/user?");
   console.log("httpClient ", httpClient);
   var cambio = '{"$unset":' + JSON.stringify(eliminar) + '}';
   console.log("cambio ", cambio);
   httpClient.put('?q={"id": ' + req.body.id + '}&' + apiKeyMLab, JSON.parse(cambio),
     function(err,resMlab,body){
     console.log("Respuesta mLab correcta");
     console.log("err ", err);
     console.log("body ", body);
     var respuesta = body;
     respuesta = !err ? body: {"msg":"error en la carga de movement en mlab"}
     res.send(respuesta);
    });
  });
});
